\documentclass{beamer}

\usetheme{simple}

\usepackage{lmodern}
\usepackage{amsbsy}
\usepackage[scale=2]{ccicons}
\usepackage{minted}  % for syntax highlighting
\usepackage{fontawesome}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows}

% Set up biblatex
\usepackage[
  doi=false,
  isbn=false,
  url=false,
  sorting=none,
  style=authoryear,
  %natbib,
  %bibstyle=numeric,
  %citestyle=reading,
  backend=biber]{biblatex}
%
% We specify the database.
\addbibresource{Clusternomics.bib}
\DeclareCiteCommand{\citejournal}
  {\usebibmacro{prenote}}
  {\usebibmacro{citeindex}%
   \usebibmacro{cite}
   \usebibmacro{journal}}
  {\multicitedelim}
  {\usebibmacro{postnote}}


% Watermark background (simple theme)
% \setwatermark{\includegraphics[height=8cm]{img/Heckert_GNU_white.png}}


\title{Clusternomics}
% \subtitle{}
\date{August 22, 2017}
\author{Evelina Gabasova \\ John Reid \\ Lorenz Wernisch}
\institute{MRC Biostatistics Unit, University of Cambridge}
% \titlegraphic{\includegraphics[width=\textwidth]{Figures/data-cartoon}}

\setbeamertemplate{itemize item}{\textemdash}
\setbeamertemplate{itemize item}{-}

\begin{document}

\tikzstyle{block} = [draw, fill=blue!20, rectangle,
  minimum height=3em, minimum width=6em, thick,
  rounded corners]
\tikzstyle{inter} = [cminimum height=3em, minimum width=6em]
\tikzstyle{lib} = [draw, rectangle, thick, rounded corners]

\maketitle

\begin{frame}{Integrative clustering}
  % \framesubtitle{\url{http://www.netlib.org/blas/}}
  \begin{columns}
    \begin{column}{0.55\textwidth}
      \includegraphics[height=.8\textheight]{Figures/cervical-clusters}

      \note{
        a, Integrative clustering of 178 core-set cervical cancer samples using
        mRNA, methylation, miRNA and copy number variation (CNV) data identifies
        two squamous-carcinoma-enriched groups (keratin-low and keratin-high) and
        one adenocarcinoma-enriched group, as shown in the feature bars (top).
        Features presented include histology, HPV clade, HPV integration status,
        UCEC-like status, APOBEC mutagenesis level, mRNA EMT score, tumour purity
        and three SMGs (KRAS, ERBB3 and HLA-A) that are significantly associated
        across the three clusters identified with iCluster (ERBB2 is presented for
        comparison purposes with its family member ERBB3).

        b, The cluster of
        clusters panel displays subtypes defined independently by mRNA, miRNA,
        methylation, reverse phase protein array (RPPA), CNV and PARADIGM data.
        C1–C6 indicate clusters. Black, sample is not represented in the cluster;
        red, sample is represented in the cluster; grey, data not available.

        c, The heatmaps show select mRNAs, miRNAs, proteins and CNVs that are either
        significantly associated with iCluster groups or have been identified as
        markers in other analyses. The heatmap colour scale bar represents the
        scale for the features presented in the heatmaps with a breakpoint of zero
        represented by white. APOBEC mut., APOBEC mutagenesis; inter.,
        intermediate.
      }
    \end{column}
    \begin{column}{0.45\textwidth}
      \begin{itemize}
        \item 178 cervical cancer samples
        \item Several contexts
          \begin{itemize}
            \item[--] Varying cluster structure
            \item[--] Different views of biology
            \item[--] Not comparable
          \end{itemize}
        \item Heterogeneous disease
        \item Subtype identification
          \begin{itemize}
            \item[--] Disease understanding
            \item[--] Clinical significance
          \end{itemize}
        \item Noisy data
        \item Natural to cluster samples
      \end{itemize}
    \end{column}
  \end{columns}

  {\tiny \citejournal{the_cancer_genome_atlas_research_network_integrated_2017}}
\end{frame}


\begin{frame}{Archetypal approaches}
  \begin{block}{Independent clusterings}
    {
      Cluster each context independently \\
      Followed by \emph{post-hoc integration}
    }
  \end{block}
  \begin{itemize}
    \item Models context specific characteristics
    \item Yields global and context specific clusterings
    \item Two-step process cannot share statistical strength
  \end{itemize}
  \begin{block}{Joint clustering}
    {
      Combine all contexts into one \\
      Perform single \emph{joint clustering}
    }
  \end{block}
  \begin{itemize}
    \item Exploits shared structure
    \item Yields only a global clustering
    \item Ignores context specific characteristics
  \end{itemize}
\end{frame}


\begin{frame}{iCluster}
  \center
  \includegraphics[width=\textwidth]{Figures/iCluster-framework}
  % \includegraphics[width=\textwidth]{Figures/iCluster-FA-model}

  \begin{columns}
    \begin{column}{0.3\textwidth}
      \begin{eqnarray*}
        \pmb{X_1} &=& \pmb{W_1 Z + \epsilon_1} \\
        \pmb{X_2} &=& \pmb{W_2 Z + \epsilon_2} \\
              &\dots& \\
        \pmb{X_M} &=& \pmb{W_M Z + \epsilon_M} \\
      \end{eqnarray*}
    \end{column}
    \begin{column}{0.7\textwidth}
      \begin{itemize}
        \item Sparse Gaussian latent variable model
        \item $k$-means clustering in latent space
        \item Membership differences treated as noise
      \end{itemize}
    \end{column}
  \end{columns}

  {\tiny \citejournal{shen_integrative_2009, shen_integrative_2012}}
\end{frame}


\begin{frame}<presentation:0>[noframenumbering]{JIVE}

  {\tiny \citejournal{hellton_integrative_2016}}
\end{frame}


\begin{frame}{Bayesian Consensus Clustering (BCC)}
  \begin{itemize}
    \item Overall clustering, $\mathcal{C}$
    \item Context-specific clusterings, $\mathcal{L}_m$
    \item For each sample, $n$:
      \begin{itemize}
        \item[--] $\mathcal{C}_n = \mathcal{L}_{mn}$ with probability $\alpha_m$
        \item[--] Otherwise, another cluster chosen uniformly at random
      \end{itemize}
  \end{itemize}
  \center
  \includegraphics[height=.5\textheight]{Figures/BCC-clusters}

  {
    \tiny
    Colours: overall clusters; symbols: context-specific clusters \\
    \citejournal{lock_bayesian_2013}
  }
\end{frame}


\begin{frame}{Multiple Dataset Integration (MDI)}
  \begin{columns}
  \begin{column}{0.5\textwidth}
    \begin{minipage}[c][.9\textheight][c]{\linewidth}
      \begin{center}
        \includegraphics[width=.7\textwidth]{Figures/MDI-independent}
      \end{center}
      \begin{align*}
        p(x) &= \sum_{c=1}^N \pi_c f(x|\theta_c) \\
        x_i|c_i,\theta &\sim F(\theta_{c_i}) \\
        c_i|\pi &\sim \textnormal{Discrete}(\pi_1, \dots, \pi_N) \\
        \pi &\sim \textnormal{Dirichlet}(\frac{\alpha}{N}, \dots, \frac{\alpha}{N}) \\
        \theta_c &\sim G^{(0)}
      \end{align*}
    \end{minipage}
  \end{column}
  \begin{column}{0.5\textwidth}
    \begin{minipage}[c][.9\textheight][c]{\linewidth}
      \begin{center}
        \includegraphics[width=.7\textwidth]{Figures/MDI-model}
      \end{center}
      \begin{align*}
        &\phi_{kl} \sim \textnormal{Gamma}(1, .2) \\
        &p(c_{i1}, \dots, c_{iK}) \propto \\
          &\prod_{k=1}^K \pi_{c_{ik}k} \prod_{k=1}^{K-1} \prod_{l=k+1}^K [1+\phi_{kl}\mathbb{I}(c_{ik}=c_{il})]
      \end{align*}
    \end{minipage}
  \end{column}
  \end{columns}

  {\tiny \citejournal{kirk_bayesian_2012}}
\end{frame}


\begin{frame}{Similarity Network Fusion (SNF)}
  \center
  \includegraphics[width=\textwidth]{Figures/SNF-method}

  {\tiny \citejournal{wang_similarity_2014}}
\end{frame}


\begin{frame}{Integrative clustering desiderata}
  \begin{block}{Share statistical strength}
    {
      Between-context influences
      \begin{itemize}
        \item Samples clustered together in one context more likely to cluster in other contexts
      \end{itemize}
    }
  \end{block}
  \begin{block}{Degrees of independence}
    {
      Contexts may not share same number of clusters \\
      Model a continuum:
      \begin{itemize}
        \item All contexts share cluster structure
        \item Each context has completely independent cluster structure
      \end{itemize}
    }
  \end{block}
  \begin{block}{Symmetry}
    {
      Wish each context to be treated equally
    }
  \end{block}
\end{frame}


\begin{frame}{Context dependent integrative clustering}
  \begin{block}{Local clusters}
    {
      Each context $m$ has its own \emph{local} mixture weights $\bm{\pi}^{(m)}$:
      \begin{align*}
        \small
        \bm{\pi}^{(m)} &\sim \text{Dirichlet}\left(\frac{\alpha_m}{K^{(m)}}, \dots, \frac{\alpha_m}{K^{(m)}} \right)
      \end{align*}
    }
  \end{block}
  \begin{block}{Global clusters}
    {
      We define the \emph{global} mixture distribution for two contexts as:
      \begin{align*}
        \small
        \bm{\rho} &\sim \text{Dirichlet}\left(\gamma \;
          \text{vec}\left(\bm{\pi}^{(1)} \otimes \bm{\pi}^{(2)}\right)\right) \\
        \begin{split}
          \small
          \bm{\pi}^{(1)} \otimes \bm{\pi}^{(2)} \; &= \;
          \begin{pmatrix}
            \pi^{(1)}_1 \pi_1^{(2)} & \pi^{(1)}_1 \pi_2^{(2)} & \cdots &
            \pi^{(1)}_1 \pi^{(2)}_K \\[0.5em]
            \pi^{(1)}_2 \pi_1^{(2)} & \pi^{(1)}_2 \pi_2^{(2)} & \cdots &
            \pi^{(1)}_2 \pi^{(2)}_K \\[0.5em]
            \vdots  & \vdots  & \ddots & \vdots  \\[0.5em]
            \pi^{(1)}_K \pi^{(2)}_1 & \pi^{(1)}_K \pi^{(2)}_2 & \cdots &
            \pi^{(1)}_K \pi^{(2)}_K
          \end{pmatrix}
          \end{split}
      \end{align*}
    }
  \end{block}
\end{frame}


\begin{frame}{Clustering structures}
  \center
  \includegraphics[height=.9\textheight]{Figures/cdc-example-pi.pdf}
\end{frame}


\begin{frame}{Simulated data}
  \begin{itemize}
    \item Two contexts
    \item Each context has two well separated clusters
    \item Simulate 100 data sets:
      \begin{itemize}
        \item[--] $p \sim \textnormal{Uniform}(0, \frac{1}{2})$
        \item[--] Sample 100 points with cluster probabilities $p, 1-p$
        \item[--] Sample 100 points with cluster probabilities $1-p, p$
      \end{itemize}
  \end{itemize}
  \center
  \includegraphics[width=\textwidth]{Figures/simulated-data}
\end{frame}


\begin{frame}{Adjusted Rand Index (ARI)}
  Given clusterings, $X = \{X_1, \dots, X_r\}$ and $Y = \{Y_1, \dots, Y_s\}$ with
  $n_{ij} =& | X_i \cap Y_j|$ and contingency table
  \begin{center}
    \begin{tabular}{c|cccc|c}
      & Y_1 & Y_2 & \dots & Y_s & Total \\
      \hline
      X_1 & n_{11} & n_{12} & \dots & n_{1s} & a_1 \\
      X_2 & n_{21} & n_{22} & \dots & n_{2s} & a_2 \\
      \vdots & \vdots & \vdots & \ddots & \vdots & \vdots \\
      X_r & n_{r1} & n_{r2} & \dots & n_{rs} & a_r \\
      \hline
      Total & b_1 & b_2 & \dots & b_s & n
    \end{tabular}
  \end{center}
  the Adjusted Rand Index (ARI) is
  \begin{equation*}
    \textnormal{ARI} =
      \frac{
        \binom{n}{2} \Sigma_{ij} \binom{n_{ij}}{2}
        - \Sigma_{i} \binom{a_{i}}{2} \Sigma_{j} \binom{b_{j}}{2}
      }{
        \frac{1}{2} \binom{n}{2} \big[ \Sigma_{i} \binom{a_{i}}{2} + \Sigma_{j} \binom{b_{j}}{2} \big]
        - \Sigma_{i} \binom{a_{i}}{2} \Sigma_{j} \binom{b_{j}}{2}
      }
  \end{equation}

\end{frame}


\begin{frame}{Simulated data: results}
  \includegraphics[width=\textwidth]{Figures/simulated-ARI}
\end{frame}


\begin{frame}{ARI: mis-specified number of clusters}
  \center
  \includegraphics[width=\textwidth]{Figures/simulated-ARI-misspecified}
\end{frame}


\begin{frame}{Discovering subtypes in breast cancer}
  \begin{itemize}
    \item 348 patients diagnosed with breast cancer (TCGA)
    \item Four different contexts:
      \begin{itemize}
        \item[] 1: RNA gene expression for 645 genes
        \item[] 2: DNA methylation for 574 probes
        \item[] 3: expression for 423 microRNAs
        \item[] 4: reverse-phase protein array (RPPA) for 171 proteins
      \end{itemize}
    \item Fit model:
      \begin{itemize}
        \item[--] 3 clusters per context
        \item[--] Maximum 18 global clusters
      \end{itemize}
  \end{itemize}

  \center
  \includegraphics[height=.45\textheight]{Figures/breast-sizes}
\end{frame}


\begin{frame}{Survival outcomes}
  \begin{center}
    \includegraphics[width=\textwidth]{Figures/breast-survival}
  \end{center}

  Significant differences between survival curves ($p = 0.038$ log-rank test)
\end{frame}


\begin{frame}{Local clusters}
  \center
  \includegraphics[height=.9\textheight]{Figures/breast-PCA-local}
\end{frame}


\begin{frame}{Global clusters}
  \center
  \includegraphics[height=.9\textheight]{Figures/breast-PCA}
\end{frame}


\begin{frame}{Two specific clusters}
  Clusters 1 (dark blue, 19 samples) and 6 (light green, 40 samples)
  \center
  \includegraphics[height=.85\textheight]{Figures/breast-PCA-two}
\end{frame}


\begin{frame}{Two specific clusters: survival}
  \begin{center}
    \includegraphics[width=\textwidth]{Figures/breast-survival-two}
  \end{center}

  Significant differences between survival curves ($p = 0.012$ log-rank test)
\end{frame}


\begin{frame}{More details}
  Paper: {\citejournal{gabasova_clusternomics:_2017}} \\

  \begin{itemize}
    \item Full model description
    \item Details of inference algorithm
    \item How to choose parameters (e.g. number of clusters)
    \item Stability analysis
    \item Case studies:
      \begin{itemize}
        \item[--] Lung cancer
        \item[--] Kidney cancer
      \end{itemize}
  \end{itemize}
\end{frame}


\begin{frame}{Conclusion}
  \begin{itemize}
    \item A probabilistic model for integrative clustering
    \item Meets desiderata
    \item Varying degrees of shared structure across contexts:
    \begin{itemize}
      \item[--] Not modelled well by other approaches
      \item[--] Present in cancer data sets
      \item[--] Clusternomics explicitly models this structure
    \end{itemize}
  \end{itemize}

  \begin{columns}
  \begin{column}{0.3\textwidth}
    \begin{minipage}[c][.5\textheight][c]{\linewidth}
      \includegraphics[height=1in]{Figures/evelina_small_bw} \\
      Evelina \\
      \faTwitter \ \texttt{@evelgab}
    \end{minipage}
  \end{column}
  \begin{column}{0.3\textwidth}  %%<--- here
    \begin{minipage}[c][.5\textheight][c]{\linewidth}
      \includegraphics[height=1in]{Figures/John} \\
      John \\
      \faTwitter \ \texttt{@\_\_Reidy\_\_}
    \end{minipage}
  \end{column}
  \begin{column}{0.3\textwidth}  %%<--- here
    \begin{minipage}[c][.5\textheight][c]{\linewidth}
      \includegraphics[height=1in]{Figures/Lorenz_Wernisch_bw} \\
      Lorenz \\
    \end{minipage}
  \end{column}
  \end{columns}

  Availability: CRAN package clusternomics
\end{frame}


\begin{frame}<beamer>[allowframebreaks]
  \frametitle{References}

  % \renewcommand*{\bibfont}{\scriptsize}
  \renewcommand*{\bibfont}{\footnotesize}
  \printbibliography
\end{frame}


\end{document}
